This repository contains the Dockerfile required to build an Atlassian JIRA image running on CentOS.

### Usage ###

* Clone the repository `git clone https://nfowlie@bitbucket.org/nfowlie/docker.git`.
* Execute `sudo docker build --rm=true --tag nathonfowlie/centos-jira:latest' to build the image.
* Execute `sudo docker run --name jira-6.3.15-x64 -d -p 8080:8080 nathonfowlie/centos-jira:latest` to create and start the docker container.

If you need easy access to the jira home directory and application logs, create the container specifying a shared volume groups:
`sudo docker run --name jira-6.3.15-x64 -v /desired/mount/location /var/atlassian/application-data/jira -v /desired/mount/location /opt/atlassian/jira/log -d -p 8080:8080 nathonfowlie/centos-jira:latest` to create and start the docker container.