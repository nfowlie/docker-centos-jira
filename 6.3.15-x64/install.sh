#!/bin/bash

yum list installed tar >/dev/null

if [ $? -ne 0 ]; then
    yum -y install tar
fi

chmod a+x atlassian-jira-6.3.15-x64.bin

bash -l ./atlassian-jira-6.3.15-x64.bin -q -varfile response.varfile

exit 0
